package com.mercury;

import static org.junit.Assert.*;

import org.junit.Test;

public class AppTest extends App {

    @Test
    public void testMultiply() {
        
        App testResult = new App();

        int result = testResult.multiply(2, 3);

        assertEquals(6, result);
    }

    @Test
    public void testAdd() {
        App testResult = new App();

        int result = testResult.add(2, 3);

        assertEquals(5, result);
    }

}
